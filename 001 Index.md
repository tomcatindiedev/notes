# Welcome to UW-Stout 2021-2022 Notes

### Math

* [[001 Modern Algebra]]
* [[001 Operating Systems]]


## TODO:
- [ ] The GCD by the LC Theorem
- [ ] Operating systems concepts
	- [ ] Kernel vs mono
	- [ ] Operating system types
	- [ ] traps
	- [ ] interrupts
	- [ ] concepts from exam
- [ ] Shell Programming concepts 
	- [ ] fork
	- [ ] pipe
	- [ ] execvp
	- [ ] pid
	- [ ] redirection

___

Tags: #index 