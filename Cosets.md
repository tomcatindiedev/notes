## Cosets
___

Cosets are associated to [[Groups]]

Motivation:
1. To get information on order of elements and subgroups
2. Constructing new groups (objects are sets).

Need: G - Group

H ≤ G
* Left cosets of H in G
* right cosets of H in G

**Notation**: Given g ϵ G
* Left coset: gH or g + H
* Right coset: Hg or H + g

***Note***: H must be a subgroup
___

**Ex:**

G = U(9) = {1,2,4,5,7,8}

H = {1,4,7} = <4>

$gH = \{gh|hϵH\}$

Left cosets:

* 1H = {1,4,7} = H
* 2H = {2,8,5}
* 4H = {4,7,1} = H
* 5H = {5,2,8}
* 7H = {7,4,1} = H
* 8H = {8,5,2}

***Note:*** Only 2 distinct subsets and right cosets are the same because they are [[Abelian]].

**Ex:**

$ℤ_8 = \{0,1,2,3,4,5,6,7\}$

$H = \{0,4\} = <4>$

* 0 + H = {0, 4}
* 1 + H = {1,5}
* 2 + H = {2,6}
* 3 + H = {3,7}
* 4 + H = {4, 1}
* 5 + H = {5, 1}
* 6 + H = {6, 2}
* 7 + H = {7, 3}

___

References: [[Groups]], [[Subgroup]]

Tags: #modernalgebra, #groupoperation 