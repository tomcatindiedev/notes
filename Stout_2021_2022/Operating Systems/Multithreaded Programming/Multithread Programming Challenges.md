### Multithread Programming Challenges

* The trend towards multicore systems continues to place pressure on system designers and application programmers to make better use of the multiple computing cores.  
* Designers of operating systems must write scheduling algorithms that use multiple  processing cores to allow a parallel execution.  
* For application programmers, the challenge is to modify existing programs as well as design new programs that are multithreaded.  
* In general, five areas present challenges in programming for multicore Systems.  

	1. **Identifying tasks**. This involves examining applications to find areas that can be divided into separate, concurrent tasks. Ideally, tasks are independent of one another and thus can run in parallel on individual cores.  
	2. **Balance**. While identifying tasks that can run in parallel, programmers must also ensure that the tasks perform equal work of equal value. In some instances, a certain task may not contribute as much value to the overall process as other tasks. Using a separate execution core to run that task may not be worth the cost.  
	3. **Data splitting**. Just as applications are divided into separate tasks, the data accessed and manipulated by the tasks must be divided to run on separate cores. 
	4. **Data dependency**. The data accessed by the tasks must be examined for dependencies between two or more tasks. When one task depends on data from another, programmers must ensure that the execution of the tasks is synchronized to accommodate the data dependency.  
	5. **Testing and debugging**. When a program is running in parallel on multiple cores, many different execution paths are possible. Testing and debugging such concurrent programs is inherently more difficult than testing and debugging single-threaded applications.

___

References: [[Multi-threading]]

Tags: #multithreading, #multithreadingchallenges