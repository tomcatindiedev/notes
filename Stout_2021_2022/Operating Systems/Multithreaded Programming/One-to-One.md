
### One-to-One  
* Each user-level thread maps to kernel thread  
* Creating a user-level thread creates a kernel thread  
* More concurrency than many-to-one  
* Number of threads per process sometimes restricted due to overhead  
* Examples  
	*  Windows  
	*  Linux  
	*  Solaris 9 and later
___

References: [[Multi-threading]]

Tags: #multithreading, #multthreadmodel