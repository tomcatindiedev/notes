* Java is a multi threaded programming language  
* Multithreaded execution is an essential feature of the Java platform.  
* Every application has at least one thread (main).  
* Main thread has the ability to create additional threads  
* A multi threaded program contains two or more parts that can run concurrently, and each part can handle different task at the same time  
* Subdivide specific operations within a single application into individual threads  
* Optimal use of the resources (Processing power)

### Defining and Starting a Thread in Java  
There are two common ways to create threads in Java.  
* Extending the Thread Class  
	* In this you need to create a new class that extends Threadclass.  
	* This approach provides more flexibility in handling multiple threads created using available methods in Thread class.  
	* This idiom is easier to use in simple applications, but is limited by the fact that your task class must be a descendant (derived from) of Thread class.  
* Implementing Runnable interface.  
	* The Runnable interface defines a single method, run, meant to contain the code executed in the thread.  
	* More commonly used. It employs a Runnable object (more general) because the Runnable object can subclass a class other than Thread.  
	* The Runnable object is passed to the Thread constructor

### Example 0: Extending the Thread Class  
```
public class HelloThreadextends Thread{  
	public void run() {  
		System.out.println("Hello from a thread!");  
	}  
		public static void main(String args[]) {  
			(new HelloThread()).start();  
		}  
}  
//oracle.com
```

___

![Extending The Thread Class](Images/ExtendingTheThreadClass.png)

![Output of example 1](Images/ExtendingTheThreadClassOutput.png)

___

![Thread priority](Images/priorityofathread.png)

![](Images/javae1.png)

![](Images/javae1o.png)

![](Images/threadpattern.png)

![](Images/javae2_1.png)

![](Images/javae2_2.png)