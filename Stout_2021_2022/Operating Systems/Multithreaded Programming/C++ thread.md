### C++ Thread
___

Defines an object that's used to observe and manage a thread of execution within an application.  
* Library: #include <thread>  
* Every thread of execution has a unique identifier of type thread::id.  
* The function this_thread::get_idreturns the identifier of the calling thread. That is, the member function thread::get_idreturns the identifier of the thread that's managed by a thread object.  
* join: Blocks until the associated thread completes.  
* Note: compile your program this way: g++ yourprogram.cpp -o yourprogramexe -fopenmp -lpthread  
* References:  
	* [MS-MSDN...](https://docs.microsoft.com/en-us/cpp/standard-library/thread-class?view=vs-2017)
___

### **How to compile C/C++ programs with std::thread and openMP in Linux.**

* **To compile programs with std::thread support, please use**:
* **g++ -std=c++11 prog1.cpp -lpthread -o prog1g**
* **For openMP support, please use**:
* **g++ -fopenmp prog1.cpp -o prog1**
* **For programs that use both (std::thread and openMP pragmas)**:
* **g++ -std=c++11 prog1.cpp -lpthread -o prog1 -fopenmp**

___
[[C++ Examples]]
	
![Example 0](Images/c++example0.png)
	
![Example 1](Images/c++example1.png)
___
	
References: [[Multi-threading]]
	
Tags: #multithreading, #c++