### Many-to-One  

* Many user-level threads mapped to single kernel thread  
* One thread blocking causes all to block  
* Multiple threads may not run in parallel on multicore  
system because only one may be in kernel at a time  
* Few systems currently use this model  
* Examples:  
	*  Solaris Green Threads

___

References: [[Multi-threading]]

Tags: #multithreading, #multthreadmodel