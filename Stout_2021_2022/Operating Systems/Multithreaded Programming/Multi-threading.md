## Multi-Threading
___

#### Table of Contents
* [[#Aspects of a process]]
* [[#Process vs Threads]]
* [[#What to Program with Threads]]
* [[#Types of Parallelism]]
* [[#Types of Multitasking]]
* [[#Why Use Threads]]
* [[#Multicore Programming]]
* [[#User-Level Threads]]
* [[#Kernel-Level Threads]]
* [[#Multithreading Models]]
	* [[Many-to-One]]  
	* [[One-to-One]]  
	* [[Many-to-Many]]
* [[#Thread Pools]]
* [[Multithread Programming Challenges]]
* [[#Thread APIs]]
* [[#Implicit Threading]]
* [[#OpenMP API]]
___
#### Aspects of a process
* [[Process]] can be viewed two ways:  
	* unit of resource ownership  
		* a process has an address space, containing program code and data  
		* a process may have open files, may be using an I/O device, etc.  
* Unit of scheduling  
	* the CPU scheduler dispatches one process at a time onto the CPU  
	* associated with a process are values in the PC, SP, and other registers

#### Process vs. Threads

* [[Process]]
	* has - address space, program code, global variables, heap, stack, OS resources (files, I/O devices, etc)
* [[Thread]]
	* has its own (other threads can access but shouldn't)
		* registers, program counter (PC)
		* stack, stack pointer (SP)
	* with other threads of the process
		* shares process resources
		* executes concurrently
		* can bloc, fork, terminate, synchronize

#### What to Program with Threads  
* good programs to multithread:  
	 * programs with multiple independent tasks:  
		 * most programs with GUIs –need to provide quick user-response while carrying out main task  
	* server which needs to process multiple independent requests simultaneously  
	* OS kernel  
	* repetitive numerical tasks —break large problem, such as weather prediction/matrix multiplication, down into small pieces and assign each piece to a separate thread  
* programs difficult to multithread:  
	* programs that don’t have multiple concurrent tasks (99% of all programs)  
	* multi-task programs that require protection between tasks (maybe one needs to run as root)

 
#### Types of Parallelism  
* In theory there are two different ways to parallelize the workload:  

	1) Data parallelism divides the data up amongst multiple cores ( threads ) and performs the same task on each subset of the data. For example dividing a large image up into pieces and performing the same digital image processing on each piece on different cores.  

	1) Task parallelism divides the different tasks to be performed among the different cores and performs them simultaneously.  

* In practice no program is ever divided up solely by one or the other of these,  
but instead by some sort of hybrid combination.

#### Types of Multitasking  
* Process-based  
	* has a self-contained execution environment.  
	* has a complete, private set of basic run-time resources; (its own memory space)  
	* Context switching from one process to another is costly.  
* Thread-based (Multithreading)  
	* Threads exist within a process —every process has at least one.  
	* Threads share the process's resources, including memory and open files.  
	* Creating a new thread requires fewer resources than creating a new process.  
	* Maximize use of CPU (multicore)

#### Why Use Threads  
* alternative –implement ad hoc switching and scheduling between tasks within process• threads are more intuitive to program,  
	* easy to switch between task contexts and coordinate between  
* alternative –create multiple processes
	* threads are cheaper to create —only need a stack and storage for registers  
	* use very little resources —don’t need new address space, global data, program code, or OS resources  
	* context switches are fast —only have to save / restore PC, SP, and registers  
	* communication between threads is easy –they share address space

![os_1](os_1.png)

### Multicore Programming  
* A recent trend in computer architecture is to produce chips with multiple cores, or CPUs on a single chip.  
* A multi-threaded application running on a traditional single-core chip would have to  interleave the threads, as shown in Figure 4.3. On a multi-core chip, however, the  threads could be spread across the available cores, allowing true parallel  processing, as shown in Figure 4.4  
![Single Core](os_2.png) ![Multi-core](os_3.png)
* For operating systems, multi-core chips require new scheduling algorithms to make  better use of the multiple cores available.  
* As multi-threading becomes more pervasive and more important ( thousands instead of tens of threads ), CPUs have been developed to support more simultaneous threads per core in hardware.

### User-Level Threads  
* provide a library of functions to allow user processes to manage (create, delete, schedule) their own threads  
	* OS kernel is not aware of the existence of threads.  
* advantages:  
	* doesn’t require modification to the OS  
	* simple representation —each thread is represented simply by a PC, registers, stack,  and a small control block, all stored in the user process’ address space  
	* simple management —creating a new thread, switching between threads, and  synchronization between threads can all be done without intervention of the kernel  
	* fast —thread switching is not much more expensive than a procedure call  
	* flexible —CPU scheduling (among those threads) can be customized to suit the needs of the algorithm
* disadvantages:  
	* lack of coordination between threads and OS kernel  
		* process as a whole getsone time slice  
		* same time slice, whether process has 1 thread or 1000 threads  
		* also —up to each thread to relinquish control to other threads in that process  
	* requires non-blocking system calls  
		* otherwise, entire process will be blocked in the kernel, even if there are runnable threads left in the process

### Kernel-Level Threads  
* kernel provides system calls to create and manage threads  

* advantages  
	* kernel has full knowledge of all threads -scheduler may choose to give a process with 10 threads more time than process with only 1 thread  
	* good for applications that frequently block (e.g., server processes with frequent inter-process communication)  
* disadvantages:  
	* slower —thread operations are significantly slower than for user-level threads  
	* significant overhead and increased kernel complexity —kernel must manage and schedule threads as well as processes -requires a full thread (task) control block (TCB) for each thread

### Multithreading Models  
* [[Many-to-One]]  
* [[One-to-One]]  
* [[Many-to-Many]]

![twolevelthreadmodel](Images/twolevelthreadmodel.png)

### Thread Pools  
* threads may not be created infinitely –overwhelm computer resources  
	*  consider web-server  
* on-demand thread creation may not be fast enough  
* thread pool  
	*  create a number ofthreads in a poolwhere they remain blocked until activated  
	*  activate when request arrives  
	*  if more requests than threads –extra requests have to wait  
* performance  
	*  controllable process size  
	*  fast response until pool exhausted  
	
### [[Multithread Programming Challenges]]
	
### Thread APIs
* Thread libraryprovides programmer with API for creating and managing threads  
* Two primary ways of implementing
	* Library entirely in user space  
	* Kernel-level library supported by the OS  
* POSIX threads (pthreads)
	* A POSIX standard (IEEE 1003.1c) API for thread creation and synchronization  
	* Common in UNIX operating systems (Solaris, Linux, MacOS X)  
	* implemented on top of “native” OS threads or as a user-level package  
* OpenMP. (will be presented in class by a team of students)  
* Other
	* Win32 threads  ([[C++ thread]])
		* one-to-one mapping  
		* implemented on Windows OSes  
* [[Java thread]] (Will be discussed in class)  
	* specified as part of Java  
	* maintained by JVM  
	* implemented on native threads or as a user-level package

### Implicit Threading  
* Growing in popularity as numbers of threads increase, program correctness more difficult with explicit threads  
* Creation and management of threads done by compilers and run-time libraries rather  than programmers  
* Two methods explored  
	* [[#Thread Pools]] –offers API to create thread pools  
	* [[#OpenMP API]]–set of compiler directives and API for programs written in C, C++, or FORTRAN for supporting parallel programming  
* Other methods:  
	* Java Threads: will be discussed in class java.util.concurrent package

### OpenMP API  
* A set of Compiler directives for multithreaded programming  
* Incremental parallelism & combines serial and parallel code in single source  
* The programmer need not specify the low-level details  
* Openmp.org

![sequentialtoparallel](Images/sequentialtoparallel.png)
___

References: [[001 Operating Systems]]

Tags: #multithreading