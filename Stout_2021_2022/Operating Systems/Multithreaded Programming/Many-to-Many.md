### Many To Many
* Allows many user level threads to be mapped to many kernel threads  
* Allows the operating system to create a sufficient number ofkernel threads  
* Solaris prior to version 9  
* Windows with the *ThreadFiber* package 

___

References: [[Multi-threading]]

Tags: #multithreading, #multthreadmodel