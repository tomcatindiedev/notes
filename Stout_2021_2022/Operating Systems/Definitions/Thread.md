
### Definition

 Is a single sequential execution stream within a process
___

References: [[001 Operating Systems]]

Tags: #multithreading, #OSDefinition
