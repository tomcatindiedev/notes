
### Definition

A process is the instance of a computer program that is being executed by one or many [[Thread]]s.
___

References: [[001 Operating Systems]]

Tags: #multithreading, #OSDefinition