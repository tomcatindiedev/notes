## Example of Permutation Matrix to Cycle
___

$\begin{bmatrix}
1&2&3&4&5&6&7\\
3&4&7&6&2&1&5
\end{bmatrix} = (1375246)$

$\begin{bmatrix}
1&2&3&4&5&6&7\\
4&3&2&5&1&7&6
\end{bmatrix} = (145)(23)(67)$
___

References: #example, #resource, #modernalgebra 

Tags: [[Permutations]]