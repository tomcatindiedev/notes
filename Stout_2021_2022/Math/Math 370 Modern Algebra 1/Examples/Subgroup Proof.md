## Example of Supgroup Proof
References: [[Subgroup]], [[Subgroup Proof Outline]], [[Groups]]

Tags: #example, #resource, #modernalgebra 
___

This example uses the [[Subgroup Proof Outline]].

#### Example

Let G be an [[Abelian]] [[Groups]]. Set H = {x  ϵ G | x<sup>2</sup> = e} ***All elements where their square is the identity.***

Proof:

**Step 0:** ***Show H is nonempty***

Check: e<sup>2</sup> = e, therefore e  ϵ H.

**Step 1:** ***Show H is closed under the operation***

Given a,b  ϵ H, we must show that a,b  ϵ H.
***Translate: we are assuming that a<sup>2</sup> = e and b<sup>2</sup> = e we want to show ab<sup>2</sup> = e.***

we have
$$ (ab)^2 = (ab)(ab)$$ 
 by commutativity
$$ (ab)^2 = a*a*b*b$$
by definition of exponents
$$ (ab)^2 = a^2*b^2$$ 
by assumption
$$ (ab)^2 = e*e$$ 
by definition
$$ (ab)^2 = e$$
Therefore ab ϵ H.

**Step 2:** ***Show H is closed under Inverse.***

Given a ϵ H we must show a<sup>-1</sup> ϵ H

***Translate: we are assuming a<sup>2</sup> = e and we must show (a<sup>-1</sup>)<sup>2</sup> = e***

we have

by property of exponents
$$(a^{-1})^2 = (a^2)^{-1}$$
by assumption
$$(a^{-1})^2 = e^{-1}$$
by property of the identity
$$(a^{-1})^2 = e$$

Therefore H is closed under inverse. Hence H≤G