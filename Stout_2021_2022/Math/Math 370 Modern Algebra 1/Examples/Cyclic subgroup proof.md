## Example of Cyclic subgroup proof
References: [[Cyclic Subgroup]], [[Subgroup Proof Outline]]

Tags: #resource , #example, #modernalgebra 
___

**Step 0**: Show \<x\> is not empty x ϵ \<x\> so it is nonempty or e = x<sup>0</sup> ϵ \<x\>.

**Step 1**: Show that \<x\> is closed under the operation. Given a,b ϵ \<x\>, we must show that a·b ϵ \<x\> ***Translate: we are assuming a = x<sup>n</sup> for some n ϵ ℤ  i.e. power of x, also that a = x<sup>m</sup> for some m ϵ ℤ. i.r power of x.***

Show that ab = x<sup>q</sup> for some q ϵ ℤ .

we have ab= x<sup>n</sup> x<sup>m</sup> = x<sup>n+m</sup>  ϵ \<x\> because of correct form (x<sup>n</sup>).

**Step 2**: Show \<x\> is closed under inverses Given a ϵ \<x\> we must show that a<sup>-1</sup> ϵ \<x\>.

***Translate: we must show that a<sup>-1</sup> = x<sup>q</sup> for some q ϵ ℤ.***

we have a<sup>-1</sup> = (x<sup>n</sup>)<sup>-1</sup> = x<sup>-n</sup> by exp prop. x<sup>-n</sup> ϵ \<x\>.

Hence \<x\> is a [[Subgroup]]