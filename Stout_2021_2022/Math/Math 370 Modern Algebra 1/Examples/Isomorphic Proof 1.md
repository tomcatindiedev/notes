### Example Proof 1
___

**Step 1:** Show 𐌘 is 1-1 by brute force 𐌘(a) = 𐌘(b) => a = b.

**Step 2:** Show 𐌘 is onto

For free since finite sets are 1-1.

or

Since everything listed, then show it preserves the operations.

**Step 3:** Given a,b ϵ ℤ<sub>3</sub> show that 𐌘 (a + b) = 𐌘(a) + 𐌘(b)

Could check all cases.
___

References: [[Groups]], [[Isomorphism]]

Tags: #example, #resource, #modernalgebra 