## Permutation Cycle to Matrix
___

$(1256)(389) = \begin{bmatrix} 1&2&3&4&5&6&7&8&9\\ 2&5&8&4&6&1&7&9&3 \end{bmatrix}$

				
$(1574)(23) = \begin{bmatrix} 1&2&3&4&5&6&7\\ 5&3&2&1&7&6&4 \end{bmatrix}$
___

References: #example, #resource, #modernalgebra 

Tags: [[Permutations]]