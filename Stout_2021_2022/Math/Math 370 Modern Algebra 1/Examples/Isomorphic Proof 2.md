### Example Proof 2
___

Let G = (ℝ, +) and G' = (ℝ > 0, ·)

Claim: G ≈ G'

**Step 0:** Define 𐌘: G -> G' by 𐌘(x) = 3<sup>x</sup>

**Step 1:** Show 𐌘 is 1-1

Given a,b ϵ G, assume 𐌘(a) = 𐌘(b) then 3<sup>a</sup> = 3<sup>b</sup> 

=> log<sub>3</sub>(3<sup>a</sup>) = log<sub>3</sub>(3<sup>b</sup>) 

=> a = b

**Step 2:** Show 𐌘 is onto

Given y ϵ G', find x such that 𐌘(x) = y. Let x = log<sub>3</sub>(y) ϵ G since y > 0. Then 𐌘(x) = 𐌘(log<sub>3</sub>(y)) = 3<sup>Log<sub>3</sub>(y)</sup> = y.

**Step 3:** Show 𐌘 preserves the operation given a,b ϵ G, we must show that 𐌘(a + b) = 𐌘(a) + 𐌘(b).

We have 𐌘(a + b) = 3<sup>a + b</sup> by prop of exp 𐌘(a) \* 𐌘(b) = 3<sup>a</sup> \* 3<sup>b</sup>
___

References: [[Groups]], [[Isomorphism]]

Tags: #example, #resource, #modernalgebra 