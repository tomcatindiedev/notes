#### The 2-step (really 3-step) subgroup test

Let G be a group and H be a subset of G to verify that H is a [[Subgroup]] of (H≤G).

We must show

1. Show that H is not empty ***typically show e ϵ H***
2. Show H is closed under the operation ***i.e. Given a,b  ϵ H, we must show that ab ϵ H***
3. Show H is closed under inverses ***i.e. Given a  ϵ H, we must show that a<sup>-1</sup> ϵ H.