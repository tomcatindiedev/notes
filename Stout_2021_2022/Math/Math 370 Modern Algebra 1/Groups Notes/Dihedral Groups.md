## Dihedral Groups
___


* Closed under operation ([[Closure Property]])
* Closed under inverse ([[Closure Property]])
* [[Associativity]]
* [[Identity]] = R<sub>0</sub>

___

***Example of top left of D<sub>n</sub>***

___

References: [[Groups]], [[ℤ Groups]], [[U-Groups]], [[Subgroup]]

Tags: #group, #modernalgebra 


