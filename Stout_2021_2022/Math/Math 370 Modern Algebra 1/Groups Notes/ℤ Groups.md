## ℤ Groups

___

n ≥ 1, n ϵ ℤ

ℤ<sub>n</sub>={0,1,2, ... , n-1} is a group under addition mod n

"ℤ<sub>n</sub>" think for now, addition mod n

___

***Example:***

ℤ<sub>7</sub> = {0,1,2,3,4,5,6}

[[Identity]] = 0

[[Inverse]]:

| Element| Inverse |
|:-:|:-:|
| 0| 0 |
| 1| 6 |
| 2| 5 |
| 3| 4 |
| 4| 3 |
| 5| 2 |
| 6| 1 |


___

References: [[Groups]], [[U-Groups]], [[Subgroup]]


Tags: #group, #modernalgebra 
