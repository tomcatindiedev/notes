## U-Groups

___


* n ϵ ℤ, n ≥ 2

* Set U(n) = {xϵℤ | 1≤x≤n and [[gcd]](x,n)=1}

* Claim: U(n) is a [[Groups]] under multiplication
	*	Closed under opertation and inverse ([[Closure Property]])
	*	[[Associativity]]
	*	[[Identity]] = 1
	*	[[Inverse]] are where 1 shows up in the table


___
**Example Multiplication Table**

|U(10)|1|3|7|9|
|:--:|:--:|:--:|:--:|:--:|
|1|1|3|7|9|
|3|3|7|9|1|
|7|7|9|1|3|
|9|9|1|3|7|


___

References: [[Groups]], [[Cyclic Subgroup]], [[Subgroup]]


Tags: #group , #grouptype, #mathdefinition , #resource, #modernalgebra 
