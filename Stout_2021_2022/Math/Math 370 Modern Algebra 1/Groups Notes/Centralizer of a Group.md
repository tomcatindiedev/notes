### Centralizer of a group

___

Elements that commute with the center.

 * Given the [[Center of a Group]]
 * Set **C<sub>G</sub>(g) = {x ϵ G | xg = gx}**

___

References: [[Groups]], [[U-Groups]], [[Center of a Group]]

Tags: #group, #groupfact, #modernalgebra 

