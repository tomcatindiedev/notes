## Isomorphism
___

*Definition of [[Isomorphic]]*

Key properties of isomorphic groups.
* Suppose G≈G<sup>'</sup> then:
1. |G| = |G<sup>'</sup>|
2. G is [[Abelian]] iff G<sup>'</sup> is abelian.
3. G is [[Cyclic]] iff G<sup>'</sup> is cyclic.

***Formal Definition***: Groups G and G<sup>'</sup> are isomorphic (G≈G<sup>'</sup>) if ∃ a function 𐌘: G -> G<sup>'</sup> that,
1. is a [[bijection]] (offsets)
2. preserves the operation (or is a "[[Homomorphism]]")

</br>

* 𐌘 is one-to-one: ∀ a,b ϵ G, if 𐌘(a) = 𐌘(b) the a = b

* 𐌘 is onto: Given y ϵ G<sup>'</sup>, ∃ x ϵ G, with 𐌘(x) = y


If $ℤ_4$ -> $ℤ_4$ (Goes to itself), it is called an [[Automorphism]] of $ℤ_4$

___

**[[Isomorphic Facts]]**

* [[Isomorphic Proof 1]]
* [[Isomorphic Proof 2]]
___

References: [[Groups]], [[Automorphism]]

Tags: #group, #groupfact, #modernalgebra 