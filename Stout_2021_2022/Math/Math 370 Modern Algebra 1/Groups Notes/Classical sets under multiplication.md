## Classical sets under multiplication

___

* *(ℝ<sup>\*</sup>, ·), (ℚ <sup>\*</sup>, ·), (ℂ <sup>*</sup>, ·)
* [[Identity]] = 1
* [[Abelian]]

___

References: [[Groups]], [[Classical sets under addition]]

Tags: #group, #modernalgebra 
