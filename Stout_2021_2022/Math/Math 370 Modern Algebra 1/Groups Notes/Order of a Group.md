## Order of a Group
___

G -> Group

| G | denotes the order i.e. is the number of elements in the set.

| U(n) | is sometimes denotes 𐌘(n) i.e. the [[Euler Ph(Totient Function)]].

* The number of positive integers less than n and [[relatively prime]] to n.
* | U(p) |  = p - 1 for some prime number p.

*Ex:*

| G | = 1 => G = {e}

| G | = 2 => G = {e, x}

| G | = 4 => G = {e, a, b, c}

___

References: [[Groups]], [[Order of an element (add)]], [[Order of an element (mult)]]

Tags: #group, #modernalgebra 

