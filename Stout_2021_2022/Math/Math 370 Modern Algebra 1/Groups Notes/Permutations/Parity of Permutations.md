## Parity of Permutations
___

~ "Even / Oddness"

**Fact**: Every permutation can be assigned a parity

* Based on 2-cycle or transpositions (ab)

**Fact**: Any cycle (permutation) can be written as a produc tof 2-cycles (ab)(cd)(ef)...etc.

e.g.

$(1234) = (14)(13)(12) = (12)(23)(34) = Odd$
___

References: #group, #resource, #modernalgebra 

Tags: [[Permutations]]