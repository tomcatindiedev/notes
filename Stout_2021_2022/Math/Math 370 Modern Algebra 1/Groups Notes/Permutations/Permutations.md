## Permutations
___

#### Definition

Rearrangement of a set of objects.

* Cominations = n!

**There are three notations**
* *Function notation*: use Greek letters α β ... etc, where ε means do nothing.
* *Matrix* notation: abc
	$$\begin{bmatrix}  
a & b & c\\  
c & a & b  
\end{bmatrix}$$

* *Cycle Notation*: (a c b)

$$\begin{bmatrix}  
a & b & c\\  
c & a & b  
\end{bmatrix} = (ab)(c)$$

**Cycle notation is not unique**
$$(123)=(231)=(312)$$

**The [[Groups]]** Let n be a positive integer. Then S<sub>n</sub> = the set of permutations of n objects. (Symmetric group on n objects)

**Claim**: S<sub>n</sub> is a group |S<sub>n</sub>| = n\!

e.g. 

$S_1 = \{1\}$

$S_2 = \{(1),(12)\}$

$S_3 = \{(1),(123),(132),(12),(13),(23)\}$

#### What makes S<sub>n</sub> a group
 * Is a set
 * Has an operation
 * [[Associativity]]
 * Identity: ε -> do nothing
 * [[Inverse]]: α<sup>-1</sup> is just "undoing" α
	 * Inverse function is one-to-one and onto

</br>

[[Multiplying Permutations]]

[[Inverses of Permutations]]

[[Order of Elements in Permutations]]

[[Parity of Permutations]]

##### Sub Topics

[[Alternating Group on n-objects]]

[[Check-Digit Scheme Based on D5]]

</br>

#### Examples
1. [[Permutation Matrix to Cycle]]
2. [[Permutation Cycle to Matrix]]
___

References: [[Groups]], [[Cyclic Subgroup]], [[U-Groups]]

Tags: #group, #groupoperation, #modernalgebra 