## Multiplying Permutations
___

Start on the right most cycle and start with 1. Go to what 1 goes to in the far right and use that as the input for the next one to the left.

1. $(1378)(243617) = (18)(247)(36)$
2. $(243617)(1378) = (16)(243)(78)$
___

References: #group, #resource, #modernalgebra  

Tags: [[Permutations]]