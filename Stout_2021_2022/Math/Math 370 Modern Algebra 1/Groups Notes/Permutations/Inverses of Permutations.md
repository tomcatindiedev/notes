## Inverses of Permutations
___

It will undo the current permutation. (abcd)<sup>-1</sup> = (adcb)

* $(123)^{-1} =  (132)$
* $(17982)^{-1} = (12897)$

* $[(125)(3867)]^{-1} = (152)(3768)$

Same method applies  as long as they are disjoint. If they are not disjoint then you should be [[Multiplying Permutations]] until they are disjoint.

___

References: #group, #resource, #modernalgebra 

Tags: [[Permutations]]