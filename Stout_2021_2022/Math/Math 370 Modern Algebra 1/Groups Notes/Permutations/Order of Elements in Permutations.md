## Order of Elements in Permutations
___

$α ϵ S_n, |α| = ?$

$α, α^2,α^3,...,α^m = (1)$

Order is equal to the number of elements

$|(29876)| = 5$

Suppose α is 42-cycle, then $|α| = 42$

**Multiple cycles**

|(12)(345)| = [[lcm]](2,3) where the cycles are disjoint.

If they are not disjoint use [[Multiplying Permutations]] to make them disjoint then apply the lcm.
___

References: #group, #resource, #modernalgebra 

Tags: [[Permutations]]