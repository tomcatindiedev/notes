## Alternating Group on n-objects
___

**Definition**: The alternating group on n-objects n ≥ 1.

* Set A<sub>n</sub> = {α ϵ S<sub>n</sub> | α is even} ϵ S<sub>n</sub>

Claim: A<sub>n</sub> ≤ S<sub>n</sub>

$S_1 = \{(1)\} \space \space S_2 = \{(1),(12)\} \space \space S_3 = \{(1),(123),(132),(12),(13),(23)\}$

$A_1 = \{(1)\} \space \space A_2 = \{(1)\} \space \space A_3 = \{(1),(123),(132)\}$

$|S_4| = 4! = 24$

$|A_4| = 12$

In general, for n ≥ 2, $|A_n| = \frac{1}{2} |S_n|$ 

</br>

**Claim**: $|A_n| + \frac{1}{2} |S_n|$ for n ≥ 2

Suppose α, β are in S<sub>n</sub>

* If α · (12) = β · (12),  then α = β

α is odd, then α · (12) is event

α is even, then α · (12) is odd

___

References: #group, #resource, #modernalgebra

Tags: [[Permutations]], [[Groups]]