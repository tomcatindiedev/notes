## Groups under addition

___

* *(ℤ, +), (ℝ, +), (ℚ, +), (ℂ, +), (Even Integers, +), etc.
* [[Identity]] = 0
* [[Inverse]] = Negatives
* [[Abelian]]

___

References: [[Groups]], [[Classical sets under multiplication]]

Tags: #group, #modernalgebra 
