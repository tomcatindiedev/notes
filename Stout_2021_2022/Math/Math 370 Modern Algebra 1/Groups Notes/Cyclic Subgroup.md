## Cyclic Subgroup
___

**Definition**: A group G is said to be cyclic if G = \<x\> for some x ϵ G i.e. , G equals at least one of it's [[Cyclic]] [[Subgroup]] s.t. an x is called the generator of the [[Groups]].

$$<a> = \{x^n | n ϵ ℤ \}$$
$$\{x^0,x^1,x^2,x^3,...,x^{-1},x^{-2}\}$$

a = all integers power of x.

* Cyclic subgroups are equal when they have the same gcd(x,n). 
* They are also in that group if they are = x · q where q is relatively prime to the order of x.
* $x^0$ is the [[Identity]]
___

#### Infinite Case

* Model: (ℤ, +)
* [[generator]]s: 1, -1
* Order of elements: 1 and -1 => 1 while the rest are infinit as long as the element is not 0

#### Finite Case
*  Model: ℤ<sub>n</sub>
*  [[generator]]s: { x | 1 ≤ x ≤ n and [[gcd]](x,n)}
*  Generators are powers [[relatively prime]] to 10.
*  All subgroups in ℤ<sub>n</sub> are [[Cyclic]].

**Facts**

[[Subgroup Facts]]

___

References: [[Groups]], [[U-Groups]], [[Subgroup]], [[Subgroup Facts]]

Tags: #group, #grouptype , #resource, #modernalgebra 
