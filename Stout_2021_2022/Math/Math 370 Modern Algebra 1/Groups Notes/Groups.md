## Groups
___

***Definition***: A **group** is a set G with a binary operation (* , +) that satisfies three properties.

1. [[Associativity]]: For any a,b,c ϵ G => (a*b)*c = a*(b*c)
2. [[Identity]]: ∃ an element (generically) e ϵ G such that ∀ a ϵ G, => a*e = e*a
3. [[Inverse]]: ∀ a ϵ G, ∃ b ϵ G such that a*b = e = b*a

**Note: The inverse is unique notation a<sup>-1</sup>**

***Not An Assumption***: Group must have [[commutativity]]

∀ a,b ϵ G, a*b=b*a

If this holds, G is said to be [[Abelian]]

</br>

**There are two categories for classical groups**
* [[Classical sets under addition]]
* [[Classical sets under multiplication]]

</br>

**Order of groups and elements**
* [[Order of a Group]]
* [[Order of an element (mult)]]
* [[Order of an element (add)]]

</br>

**Center and Centralizer**

* [[Center of a Group]]
* [[Centralizer of a Group]]

</br>
</br>

### Types of Groups
1. [[Dihedral Groups]]
2. [[ℤ Groups]]

___
References: [[U-Groups]], [[Cyclic Subgroup]], [[Subgroup]], [[Center of a Group]], [[Centralizer of a Group]], [[Permutations]]

Tags: #group, #grouptype, #mathdefinition, #resource, #modernalgebra 

[[001 Modern Algebra]]

