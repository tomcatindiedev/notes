## Subgroup

___


A subgroup of a [[Group]] G is a subset H of G that is itself a group under the operation of G.

* Notation: H≤G

If H is strictly smaller, called "proper" subgroup.

{e} ≤ G

***Note:*** subgroup needs to have the same operation as it's derived parent group.

#### Proof
[[Subgroup Proof Outline]]
**Exampe of subgroup proof**

[[Subgroup Proof]]

___

References: [[Groups]], [[Cyclic Subgroup]], [[U-Groups]]

Tags: #group, #grouptype, #mathdefinition, #resource, #modernalgebra 

