## Order of an Element

___

Defined [[Order of an element (mult)]]: For a "Generic" multiplicative group G. 
* Notation: |x| = The least positive integer n such that x<sup>n</sup>=e if this exists. Otherwise, say infinite order.

In general [[Order of element e]] will be 1.

*Ex:*

U(9) = {1,2,3,5,7,8}

| U(9) | = 6

Identity = 1

* | 1 | = 1
* | 2 | = 6
* | 4 | = 3


___


References: [[Groups]]

Tags: #group, #modernalgebra 

