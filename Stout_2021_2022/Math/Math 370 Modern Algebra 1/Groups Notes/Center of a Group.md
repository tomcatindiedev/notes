### Center of a group

___

Elements that have [[commutativity]] with all other elements. 

The center of the group
* G - any group

ℤ (G) = {xϵG| xg=gx ∀ g ϵ G}

***Elements that commute with all other elements***

*Ex:*

D<sub>3</sub> = {R<sub>0</sub>,R<sub>1</sub>,R<sub>2</sub>, F, R<sub>1</sub>F, R<sub>2</sub>F}

ℤ(D<sub>3</sub>) = {R<sub>0</sub>}

___

References: [[Groups]], [[U-Groups]], [[Centralizer of a Group]]

Tags: #group, #groupfact, #modernalgebra 
