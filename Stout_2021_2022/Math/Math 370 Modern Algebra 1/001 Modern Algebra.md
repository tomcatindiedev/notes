# Modern Algebra
___

### Groups
1. [[Groups]]
	1. [[U-Groups]]	
	2. [[Subgroup]]
	3. [[Cyclic Subgroup]]	
	4. [[Permutations]]
	5. [[Isomorphism]]
		1. [[Automorphism]]
2. [[Cosets]]


### Group Facts
1. [[Group Facts]]
2. [[Order of element e]]
3. [[Subgroup Facts]]
4. [[Isomorphic Facts]]

### Resources
1. [[Dihedral Tables]]
2. [[Check Digit]]
3. [[Check-Digit Scheme Based on D5]]

### Definitions

1. [[Abelian]]
2. [[Associativity]]
3. [[Center of a Group]]
4. [[Centralizer of a Group]]
5. [[Closure Property]]
6. [[commutativity]]
7. [[Cyclic]]
8. [[Euclid's Algorithm]]
9. [[Euler Ph(Totient Function)]]
10. [[generator]]
11. [[Inverse]]
12. [[Order of an element (mult)]]
13. [[Order of an element (add)]]
15. [[relatively prime]]
15. [[The GCD by LC Theorem]]

### Homeworks
1. [[Homework 11]]


___

References: [[001 Index]]

Tags: #index, #modernalgebra
