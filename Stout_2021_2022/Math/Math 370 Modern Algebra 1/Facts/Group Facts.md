### Properties:
1. The identity is unique.
2. Inverses are unique.

___

##### Cancellation

Cancellation can only be done if the element is in an equivalent position.

_Ex:_
		$$a*c = a*b \space\space or\space\space c*a=c*b$$
		
Can never cross cancel elements

___
##### Exponent notation in a group under multiplication

* Concentration: ab vs a\*b or a·b
* a<sup>3</sup> = a·a·a = a\*a\*a
* a<sup>0</sup> = Identity
* a<sup>-4</sup> = (a·a·a·a)<sup>-1</sup> = a<sup>-1</sup>·a<sup>-1</sup>·a<sup>-1</sup>·a<sup>-1</sup> = (a<sup>-1</sup>)<sup>4</sup>

___
##### Additive: Usual notation

* 0 = ID
* Negatives for inverses
* 3x = x\*x\*x

___

##### Inverse of a multiplication

a,b ϵ G then (ab)<sup>-1</sup> = a<sup>-1</sup> \* b<sup>-1</sup>
___

Tags: #group, #groupfact, #modernalgebra 

References: [[Groups]], [[Subgroup Facts]]