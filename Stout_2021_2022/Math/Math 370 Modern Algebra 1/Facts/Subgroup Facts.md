* \<x\> = \<x<sup>-1</sup>\>
* |\<x\>| = |x|
* |x| = |x<sup>-1</sup>|
* Suppose x<sup>n</sup> = e for some positive integer n, then |x| | n

**Cyclic**

* Cyclic subgroups are [[Abelian]]
* Any cyclic group is [[Isomorphic]] to either some ℤ<sub>n</sub> or (ℤ, +). *Finite and Infinite respectively*
* All unique subgroups of n are the factors of n.
* Order of a subgroup divides n each divisor occurs. If d|n then |\<d\>| = n/d
___


References: [[Cyclic Subgroup]], [[Groups]]

Tags: #groupfact , #resource, #modernalgebra 