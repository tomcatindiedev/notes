**Observations:**

1. 𐌘 (e<sub>G</sub>) = e<sub>G'</sub>
2. ∀ x ϵ G -> 𐌘 (x<sup>-1</sup>) = 𐌘(x)<sup>-1</sup>
3. ∀ x ϵ G, n ϵ ℤ -> 𐌘 (x<sup>n</sup>) = 𐌘(x)<sup>n</sup>
4. ∀ x ϵ G -> |𐌘(x)| = |x|
___

Tags: #groupfact, #resource, #modernalgebra 

References: [[Groups]], [[Isomorphism]]