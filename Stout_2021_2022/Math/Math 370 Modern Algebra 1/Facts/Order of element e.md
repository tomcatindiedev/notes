$| e | = 1$

*Is the only element of order 1.*

___


Tags: #groupfact, #resource, #modernalgebra 

References: [[Groups]]
