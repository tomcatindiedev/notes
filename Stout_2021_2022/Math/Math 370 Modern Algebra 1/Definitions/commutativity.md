
### Definition

relating to number operations of addition and multiplication, stated symbolically: a+b=b+a and ab=ba. From these laws it follows that any finite sum or product is unaltered by reordering its terms or factors.

___

References: [[Groups]]

Tags: #mathdefinition, #modernalgebra 