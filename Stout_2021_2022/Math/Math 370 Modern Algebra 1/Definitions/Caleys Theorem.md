
### Definition

Any finite group is [[Isomorphic]] to a group of [[Permutations]].
___

References: [[Groups]]

Tags: #mathdefinition 
