
### Definition

(of a group) having members related by a commutative operation

___

References: [[Groups]]

Tags: #mathdefinition, #modernalgebra 
