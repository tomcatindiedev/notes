
| T | R<sub>0</sub> | R<sub>1</sub> | R<sub>2</sub> | R<sub>3</sub> |
|---|---|---|---|---|
| R<sub>0</sub> | R<sub>0</sub> | R<sub>1</sub> | R<sub>2</sub> | R<sub>3</sub> |
| R<sub>1</sub> | R<sub>1</sub> | R<sub>2</sub> | R<sub>3</sub> | R<sub>0</sub> |
| R<sub>2</sub> | R<sub>2</sub> | R<sub>3</sub> | R<sub>0</sub> | R<sub>1</sub> |
| R<sub>3</sub> | R<sub>3</sub> | R<sub>0</sub> | R<sub>1</sub> | R<sub>2</sub> |



| G | R<sub>0</sub> | R<sub>1</sub> | R<sub>2</sub> | R<sub>3</sub> | F |R<sub>1</sub>F |  R<sub>2</sub>F | R<sub>3</sub>F |
|---|---|---|---|---|---|---|---|---|
| R<sub>0</sub> | R<sub>0</sub> | R<sub>1</sub> | R<sub>2</sub> | R<sub>3</sub> | F | R<sub>1</sub>F | R<sub>2</sub>F | R<sub>3</sub>F |
| R<sub>1</sub> | R<sub>1</sub>| R<sub>2</sub>| R<sub>3</sub> | R<sub>0</sub> | R<sub>1</sub>F | R<sub>2</sub>F| R<sub>3</sub>F| F |
| R<sub>2</sub> | R<sub>2</sub>| R<sub>3</sub> | R<sub>0</sub> | R<sub>1</sub> | R<sub>2</sub>F | R<sub>3</sub>F | F | R<sub>1</sub>F |
| R<sub>3</sub> | R<sub>3</sub>| R<sub>0</sub> | R<sub>1</sub> | R<sub>2</sub>| R<sub>3</sub>F | F | R<sub>1</sub>F | R<sub>2</sub>F |
| F | F | R<sub>3</sub>F | R<sub>2</sub>F | R<sub>1</sub>F | R<sub>0</sub> | R<sub>3</sub> | R<sub>2</sub> | R<sub>1</sub>|
| R<sub>1</sub>F | R<sub>1</sub>F | F | R<sub>3</sub>F | R<sub>2</sub>F | R<sub>1</sub> | R<sub>0</sub>| R<sub>3</sub>| R<sub>2</sub>|
| R<sub>2</sub>F | R<sub>2</sub>F| R<sub>1</sub>F| F | R<sub>3</sub>F | R<sub>2</sub>| R<sub>1</sub>| R<sub>0</sub>| R<sub>3</sub>|
| R<sub>3</sub>F | R<sub>3</sub>F| R<sub>2</sub>F | R<sub>1</sub>F |  F| R<sub>3</sub>| R<sub>2</sub>|R<sub>1</sub> | R<sub>0</sub>|

___

References: [[Groups]]

Tags: #resource, #modernalgebra 