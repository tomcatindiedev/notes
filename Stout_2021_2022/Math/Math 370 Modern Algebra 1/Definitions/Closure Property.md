
### Definition

The closure property means that a set is closed for some mathematical operation. That is, a set is closed with respect to that operation if the operation can always be completed with elements in the set. Thus, a set either has or lacks closure with respect to a given operation.

Additionally if it is closed under inverse then the inverse of any given element is also in the group.

___

References: [[Groups]]

Tags: #mathdefinition, #modernalgebra 