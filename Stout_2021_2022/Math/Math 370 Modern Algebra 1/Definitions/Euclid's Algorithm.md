### Definition

$$Inverse\space of\space b\space in\space U(a)$$

$$a = nb+r$$
$$where\space n\space is\space how\space many\space time\space b\space goes\space into\space a\space and\space r\space is\space the\space remainder.$$

**Example:**

$$Find \space the \space inverse\space of \space3 \space in \space U(80)$$

$3150 = 4*660+510$

$600 = 1*510+150$

$510 = 3*150+60$

$150 = 2*60+30$

$60 = 2*30+0$

</br>

$30 = 150-2*60$

$30 = 150-2(510-3*150)$

$30 = 150-2*510+6*150$

$30 = 7*150-2*510$

$30 = 7(660-510)-2*510$

$30 = 7*660-7*510-2*510$

$30 = 7*660-9*510$

$30 = 7*660-9(3150+36*660)$

$30 = 7*660-9*3150+36*660$

$30 = 43*660-9*3150$
___

References: [[Groups]]

Tags: #mathdefinition , #resource, #modernalgebra 