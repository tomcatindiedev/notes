
### Definition
∃ integers s.t. 1 = sa + tb.
___

References: [[Groups]]

Tags: #mathdefinition, #resource, #modernalgebra 