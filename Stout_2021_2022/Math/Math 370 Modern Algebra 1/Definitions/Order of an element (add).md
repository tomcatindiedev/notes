
### Definition

For a "Generic" additive group

* Notation: |x| = least positive integer n with nx=0 => x+x+x+x+... = 0
___

References: [[Groups]]

Tags: #mathdefinition, #modernalgebra 