### Check Digit
___

**ISBN**
* **10**

$\begin{bmatrix}0&1&9&8&5&2&6&6&3\\10&9&8&7&6&5&4&3&2  \end{bmatrix}*$

$\begin{bmatrix}0&9&72&56&30&10&24&18&6 \end{bmatrix}+ = 225$

$255 % 11 = 5$

$11 - 5 = 6$

Meaning that the check digit is **6**

* **13**

$\begin{bmatrix}9&7&8&1&8&6&1&9&7&8&7&6\\1&3&1&3&1&3&1&3&1&3&1&3 \end{bmatrix}*$

$\begin{bmatrix}9&21&8&18&1&27&7&24&7&18 \end{bmatrix}+ = 151$

$151 % 10 = 1$

$10 - 1 = 9$

Meaning that the check digit is **9**
___

References: [[Groups]]

Tags: #group, #resource, #modernalgebra 