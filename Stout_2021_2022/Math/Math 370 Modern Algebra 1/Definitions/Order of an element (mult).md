
### Definition
For a "Generic" multiplicative group G
* |x| = The least positive integer n s.t. x<sup>n</sup> = e if this exists. Otherwise, say infinite order.

In general [[Order of element e]] is 1.
___

References: [[Groups]]

Tags: #mathdefinition , #modernalgebra 