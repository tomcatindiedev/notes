
### Definition

Totient function 𐌘(n) can be simply defined as the number totatives of n. For example, there are eight totatives of 24 (1, 5, 7, 11, 13, 17, 19, and 23), so 𐌘(24) = 8.

___

References: [[Groups]]

Tags: #mathdefinition, #modernalgebra 
