
### Definition

A element that is used to generate a subgroup, usually a [[Cyclic Subgroup]].
___

References: [[Groups]]

Tags: #mathdefinition, #modernalgebra 
