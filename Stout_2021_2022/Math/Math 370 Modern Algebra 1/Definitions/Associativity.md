### Definition

This property states that when three or more are added (or multiplied), the sum (or the ) is the same regardless of the grouping of the (or the multiplicands).

___

References: [[Groups]]

Tags: #mathdefinition, #modernalgebra 
