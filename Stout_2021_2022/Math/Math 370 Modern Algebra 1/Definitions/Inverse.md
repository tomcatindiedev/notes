
### Definition of group inverses

* Since gcd(a,n)=1, by [[The GCD by LC Theorem]]
The "b" (i.e. a<sup>-1</sup>) is the s (sort of), take ***s mod n***.

* Finding the inverse can be done by using [[Euclid's Algorithm]]
___

References: [[Groups]]

Tags: #mathdefinition, #modernalgebra 