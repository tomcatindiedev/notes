
### Definition

Two groups G and G<sup>'</sup> are said to be *isomorphic* (G≈G<sup>'</sup>) if their multiplication tables are the same up to choice of symbols and ordering.
___

References: [[Groups]]

Tags: #mathdefinition, #modernalgebra 
