
### Definition
An element of the set which leaves unchanged every element of the set when the operation is applied.
___

References: [[Groups]]

Tags: #mathdefinition, #modernalgebra 
