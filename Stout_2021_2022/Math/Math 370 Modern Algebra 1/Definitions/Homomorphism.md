
### Definition

A transformation of one set into another that preserves in the second set the relations between elements of the first.

* 𐌘(e) = e<sup>1</sup>

* $𐌘(x^{-1})$ = $𐌘(x)^{-1}$

* $𐌘(x^{n})$ = $𐌘(x)^{n}$

___

References: [[Groups]]

Tags: #mathdefinition 
