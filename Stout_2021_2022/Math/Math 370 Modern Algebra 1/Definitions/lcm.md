
### Definition
The smallest positive integer that is divisible by both _a_ and _b_.

___

References: [[Groups]]

Tags: #mathdefinition, #modernalgebra 
