
### Definition

Occuring in cycles; regularly repeated.
___

References: [[Groups]]

Tags: #mathdefinition, #modernalgebra 
