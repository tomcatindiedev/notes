### Definition
When two numbers have no common factors other than 1. In other words there ***is*** no value that you could exactly divide them both (without any remainder).
___

References: [[Groups]]

Tags: #mathdefinition, #modernalgebra 