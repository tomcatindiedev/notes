## MATH-370 Homework 11
[[001 Modern Algebra]]

1. **(1 pt.) Consider the group S<sub>4</sub>.**

	1. **Give an example of an element of order 5 or explain why no such element exists.**

		$None$
	
	2.  **Give an example of an element of order 6 or explain why no such element exists.**

		There is no such element because any element alone will have an order of 1, 2, 3, or 4. So an order of 6 can not be achieved by a single element.
	
	3.   **Give an example (explicitly listing all members) of a subgroup of order 5 or explain why no such subgroup exists.**

	     $A_4 = \{(1), (123),(124),(132),(134),(142),(143),(234),(243)\}$

	4.  **Give an example (explicitly listing all members) of a subgroup of order 6 or explain why no such subgroup exists.**

		There are no subgroups that can have an order greater than the parent group. Since the subgroup is derived from the group then the subgroup has less than or equal to the same number of elements.
	
2. **(2 pts.) Consider the group U(10) = {1,3,7,9}. For each x ∈U(10), define a permutation T<sub>x</sub> of the elements {1,3,7,9} by T<sub>x</sub>(a) = xa (multiplication in U(10)). For example, the permutation/function T<sub>3</sub> has values as follows: T<sub>3</sub>(1) = 3, T<sub>3</sub>(3) = 9, T<sub>3</sub>(7) = 1, and T<sub>3</sub>(9) = 7. So, in cycle notation, T<sub>3</sub> = (1397) ∈S9.**

	1.	**Identify the other three permutations T<sub>1</sub>, T<sub>7</sub>, and T<sub>9</sub> in cycle notation.**
	
		$T_1 = (1379)$ , $T_7 = (7193) = (1937)$ , $T_9 = (9731) = (1973)$
	
	2. **Consider the subset $H = {T_1,T_3,T_7,T_9} ⊂ S_9$, which is in fact a subgroup. Write out the multiplication table for H in two ways: once using cycle notation and then a second time using T-notation.**
		
		Unsure?

		|H,\*	  |$(1379)$ |$(1397)$ |$(1937)$ |$(1973)$ |
		| :-:		| :-:		| :-:		| :-:		| :-:		|
		|$(1379)$ |$(17)(39)$ |$(193)$ |$(173)$ |$(1)$ |
		|$(1397)$ |$(173)$ |$(19)(37)$ |$(179)$ |$(379)$ |
		|$(1937)$ |$(397)$ |$(173)$ |$(13)(79)$ |$(179)$ |
		|$(1973)$ |$(1)$ |$(179)$ |$(139)$ |$(17)(39)$ |

		|H,\*	  |$T_1$ |$T_3$ |$T_7$ |$T_9$ |
		| :-:		| :-:		| :-:		| :-:		| :-:		|
		|$T_1$ |$T_1$ |$T_3$ |$T_7$ |$T_9$ |
		|$T_3$ |$T_3$ |$T_9$ |$T_7$ |$T_9$ |
		|$T_7$ |$T_7$ |$T_3$ |$T_1$ |$T_9$ |
		|$T_9$ |$T_9$ |$T_3$ |$T_7$ |$T_1$ |
	
	3.  Identify the identity element and the inverse of each element in H. Do this in both notations.
		Unsure?
		

	4. Is H cyclic? If so, identify a generator (in both notations).
		Unsure?


3. **(1.5 pts.) Briefly but explicitly explain why the following pairs of groups are not isomorphic.**

	1. **S<sub>6</sub> and Z<sub>720<sub>**
	
		Both groups have different orders. For them to be isomorphic they need to have the same order.
		
	2. **Z<sub>4</sub> and the subgroup H = {(1),(12)(34),(13)(24),(14)(23)}≤A4.**
	
		Yes they are isomorphic, they both have the same order and there can be a function that is 1-1 and onto.
	
	3. **U(9) and U(15).**
	
		|U(9)| = 6 and |U(15)| = 8. With their orders not being the same they can not be isomorphic.
	
	4.  **D<sub>4</sub> and the group of Quaternions defined on p. 95 of the textbook.**
	
	5.  **(Z,+) and (R,+)**
	
		With there being infinite order for both groups, each element has the potential to directly map to another in the other group. It is also possible to create a function that preserves the operation and maps the elements from one group to the other. Example 1 = 1/1 , 2 = 1/2, 3 = 1/3, ...etc.
	
	6.  **Z<sub>7</sub> and U(7).
	
		They do not have the same order. This mean it would be impossible to map all of Z<sub>7</sub> to all the elements of U(7).
	
4. **(0.5 pts.) The groups S<sub>3</sub> and D<sub>3</sub> are isomorphic. Write out a multiplication table for S<sub>3</sub>  with elements ordered so that the structure “clearly” matches that for D<sub>3</sub> (as in the table on the class handout). Give an explicit isomorphism that corresponds to this ordering. Note: You do not need to prove it is an isomorphism, but be sure it is.**
	
	$(1) - R_0, (12) - R_1, (13) - R_2, (23) - F,  (123) - R_1F, (132) - R_2F$
	
	|$S_3$|(1)|(12)|(13)|(23)|(123)|(132)|
	|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
	|**(1)**    |(1)|(12)|(13)|(23)|(123)|(132)|
	|**(12)**  |(12)|(13)|(1)|(123)|(132)|(23)|
	|**(13)**  |(13)|(1)|(12)|(132)|(23)|(123)|
	|**(23)**  |(23)|(132)|(123)|(1)|(13)|(12)|
	|**(123)**|(123)|(23)|(132)|(12)|(1)|(13)|
	|**(132)**|(132)|(123)|(23)|(13)|(12)|(1)|
	
5. (1 pts.) Consider the groups Z4, U(5), U(8), and U(12) which are all abelian groups of order 4. Write out the “multiplication” tables for each one. Then compare them and deter- mine which groups are isomorphic to which. For those that are, give an explicit isomorphism between the groups. Note: you do not need to prove these are isomorphisms, just define the functions.

	|U(5)|1|2|3|4|
	|:-:|:-:|:-:|:-:|:-:|
	|**1**|1|2|3|4|
	|**2**|2|4|1|3|
	|**3**|3|1|4|2|
	|**4**|4|3|2|1|
	
	|U(8)|1|3|5|7|
	|:-:|:-:|:-:|:-:|:-:|
	|**1** |1|3|5|7|
	|**3**|3|1|7|5|
	|**5**|5|7|1|3|
	|**7**|7|5|3|1|
	
	|U(8)|1|5|7|11|
	|:-:|:-:|:-:|:-:|:-:|
	|**1** |1|5|7|11|
	|**5**|5|1|11|7|
	|**7**|7|11|1|5|
	|**11**|11|7|5|1|
	
	They are all isomorphic I will represent this by U(5) ≈ U(8) ≈ U(12)
	
	First U(5) ≈ U(8):
	
	1 -> 1, 2 -> 5, 3 -> 3, 4 -> 7
	
	Then U(8) ≈ U(12):
	
	1 -> 1, 3 -> 5, 5 ->  7, 7 -> 11
	
6. **(1.5 pts.) Consider the groups G = (3Z,+) and G′ = (7Z,+). Prove that G and G′ are isomorphic by constructing an explicit isomorphism and verifying that it satisfies the necessary properties. Make sure to show that your function is well-defined.**
	
	

7. **(1.5 pts.) Let φ : G → G′ be an isomorphism and x ∈ G with |x| = 6. Determine the following:**
	
		a) |x2|= 3
	
		b) |x3|= 2
	
		c) |φ(x)|= 6
	
		d) |φ(x−1)|= 1
	
		e) |φ(x)6|= 1
	
		f) |φ(x3)|= 2

8. **(1 pt.) Looking ahead questions - see notes or textbook.**
	
		(a) An isomorphism from a group G to itself is called an of G.
		Automorphism
	
		(b) True/False: The set Aut(G) of automorphisms of a group G is itself a group under function composition.
		True
	
		(c) How many automorphisms of Z6 are there?
		2
		(d) How many automorphisms of Z8 are there?
		4